from django.http import HttpResponse
from django.shortcuts import render

def webpage1(request):
    return render(request, 'pageSelamatDatang.html')

def webpage2(request):
    return render(request, 'index.html')

def webpage3(request):
    return render(request, 'raviSesuaiWireframe.html')

def webpage4(request):
    return render(request, 'raviSesuaiWireframeHTMLTambahan.html')   
